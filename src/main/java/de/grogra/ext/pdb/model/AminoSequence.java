/*
 * Created on 04.06.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package de.grogra.ext.pdb.model;

import java.util.Enumeration;
import java.util.Vector;

/**
 * @author fdill, mdube
 * This class models a protein which is a whole pdb file.
 * It holds the single amino acids in a vector. 
 * 
 * RH20060629 changed variable name 'enum' to 'e' to avoid problems with java 1.5
 */

public class AminoSequence {

	/**
	 * 
	 * @uml.property name="aminoAcids"
	 */
	Vector aminoAcids;
	
	/**
	 * a counter for statistics. Is incremented with every creation.
	 */
	public static int numberOfSequences=0;

	public AminoSequence() {
		numberOfSequences++;
		aminoAcids = new Vector();
	}

	/**
	 * @return - a vector containing the amino acids belonging to this sequence.
	 * 
	 * @uml.property name="aminoAcids"
	 */
	public Vector getAminoAcids() {
		return aminoAcids;
	}

	/**
	 * 
	 * @param acid - an acid to be added to this sequence
	 */
	public void addAminoAcid(AminoAcid acid){
		aminoAcids.add(acid);
	}
	/**
	 * Prints the current sequence with the amino acid names and the atom names to the standard output.
	 *
	 */
	public void textOutput(){
		AminoAcid acid;
		for(Enumeration e = aminoAcids.elements(); e.hasMoreElements(); ){
			acid = (AminoAcid)e.nextElement();
			System.out.println(acid.getName() + ": ");
			System.out.println(acid.getAtoms().toString());
		}
	}
}
