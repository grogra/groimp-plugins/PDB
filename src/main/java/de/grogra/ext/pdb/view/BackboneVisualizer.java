
package de.grogra.ext.pdb.view;

import java.util.HashMap;

import de.grogra.ext.pdb.model.IProtein;
import de.grogra.graph.Graph;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Cylinder;
import de.grogra.imp3d.shading.RGBAShader;
import de.grogra.pf.ui.Command;
import de.grogra.pf.ui.Context;

/**
 * @author fdill, mdube
 * This class is responsible for visualizing the data model as a backbone model.
 * The backbone model only shows the N, C-alpha, C atoms of the amino acids and the bonds between them. 
 * 
 * RH20060629 changed AColor to RGBAShader
 * RH20060629 changed Color parameters to int 0xAARRGGBB
 * RH20060629 changed "cylinder.radius = ..." to "cylinder.setRadius(...)"
 */
public class BackboneVisualizer implements Visualizer, Command
{

	private IProtein protein;

	private final float RADIUS = 0.3f;
	private final int BACKBONEATOMS = 2;

	private static HashMap colors = new HashMap ();

	/**
	 * Creation of a HashMap for coloring the acids. The colors are arbitrary.
	 * @param protein - the data model representing the read *.pdb file.
	 */
	public BackboneVisualizer (IProtein protein)
	{
		this.protein = protein;
		colors.put ("ALA", RGBAShader.YELLOW);
		colors.put ("ARG", RGBAShader.BLUE);
		colors.put ("ASN", RGBAShader.CYAN);
		colors.put ("ASP", RGBAShader.RED);
		colors.put ("CYS", RGBAShader.GRAY);
		colors.put ("GLN", RGBAShader.GREEN);
		colors.put ("GLU", RGBAShader.PINK);
		colors.put ("GLY", RGBAShader.MAGENTA);
		colors.put ("HIS", RGBAShader.ORANGE);
		colors.put ("ILE", new RGBAShader (0xFF800000));
		colors.put ("LEU", new RGBAShader (0xFF808000));
		colors.put ("LYS", new RGBAShader (0xFF80C0FF));
		colors.put ("MET", new RGBAShader (0xFF008000));
		colors.put ("PHE", new RGBAShader (0xFF000080));
		colors.put ("PRO", new RGBAShader (0xFF008080));
		colors.put ("SER", new RGBAShader (0xFF800080));
		colors.put ("THR", new RGBAShader (0xFF400000));
		colors.put ("TRP", new RGBAShader (0xFFFF80C0));
		colors.put ("TYR", new RGBAShader (0xFF80FFC0));
		colors.put ("VAL", new RGBAShader (0xFFFFC080));
		colors.put ("UNK", new RGBAShader (0xFFFFFFC0));
	}

	/**
	 * @see de.grogra.ext.pdb.Visualizer#createGraph()
	 **/
	public Node createGraph ()
	{
		Node root = new Node ();
		Cylinder cylinder;
		RGBAShader currentColor;
		//sequences
		Node seqNode = new Node ();
		for (int seq = 0; seq < protein.getNumberOfSequences (); seq++)
		{
			//acids
			Node acidNode = new Node ();
			for (int acid = 0; acid < protein.getNumberOfAcids (seq); acid++)
			{
				//atoms
				currentColor = (RGBAShader) colors.get (protein.getAcidName (
						seq, acid));
				for (int atom = 0; atom <= BACKBONEATOMS; atom++)
				{
					cylinder = new Cylinder ();
					cylinder.setTransform (protein.getXCoord (seq, acid, atom),
							protein.getYCoord (seq, acid, atom), protein
									.getZCoord (seq, acid, atom));
					cylinder.setRadius (RADIUS);
					cylinder.setShader (currentColor);
					if (acid > 0 && atom == 0)
					{
						cylinder.setEndPoints
							(protein.getXCoord (seq, acid, atom),
							 protein.getYCoord (seq, acid, atom),
							 protein.getZCoord (seq, acid, atom),
							 protein.getXCoord (seq, acid - 1, BACKBONEATOMS),
							 protein.getYCoord (seq, acid - 1, BACKBONEATOMS),
							 protein.getZCoord (seq, acid - 1, BACKBONEATOMS));
						acidNode.addEdgeBitsTo (cylinder, Graph.SUCCESSOR_EDGE,
								null);
					}
					else if (atom > 0)
					{
						cylinder.setEndPoints
						(protein.getXCoord (seq, acid, atom),
						 protein.getYCoord (seq, acid, atom),
						 protein.getZCoord (seq, acid, atom),
						 protein.getXCoord (seq, acid - 1, atom),
						 protein.getYCoord (seq, acid - 1, atom),
						 protein.getZCoord (seq, acid - 1, atom));
						acidNode.addEdgeBitsTo (cylinder, Graph.SUCCESSOR_EDGE,
								null);
					}
				}
				seqNode.addEdgeBitsTo (acidNode, Graph.BRANCH_EDGE, null);
			}
			root.addEdgeBitsTo (seqNode, Graph.BRANCH_EDGE, null);
		}
		return root;
	}

	public String getCommandName ()
	{
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * This method is executed by choosing the corresponding menu entry "Backbone Model" 
	 */
	public void run (Object info, Context ctx)
	{
		System.out.println ("running command Backbone Modell...");
		//		Editor3D editor = (Editor3D) ctx.getEditor ();
		//		editor.openEditor(createGraph());
		//		System.out.println("done");		
	}
}
