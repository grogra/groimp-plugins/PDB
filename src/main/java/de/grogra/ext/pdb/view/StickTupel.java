/*
 * Created on 11.07.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package de.grogra.ext.pdb.view;

/**
 * @author fdill, mdube
 * This is a datatype that represents the bond between two atoms.
 * It contains the indices of these atoms in the vector of the @link AminoAcid.
 * It is used by @link BallStickModel.
 */

public class StickTupel {
	int startAtomIndex;
	int endAtomIndex;
	/**
	 * @param start - the index of the first atom
	 * @param end - the index of the second atom 
	 */
	public StickTupel(int start, int end){
		startAtomIndex = start;
		endAtomIndex = end;
	}
	
	/**
	 * @return - the index of the second atom
	 */
	public int getEndAtomIndex() {
		return endAtomIndex;
	}

	/**
	 * @return - the index of the first atom
	 */
	public int getStartAtomIndex() {
		return startAtomIndex;
	}

}
